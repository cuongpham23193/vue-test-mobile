module.exports = {
  mode: 'jit',
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        blue: {
          450: '#3182CE',
        },
        green: {
          450: '#319795',
          550: '#81E6D9',
        },
        gray: {
          450: '#718096',
        },
      },
    },
    boxShadow: {
      header: '0px 3px 6px #00000029',
      footer: '0px -1px 3px #00000033',
    },
    fontFamily: {
      lato: ['lato'],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
